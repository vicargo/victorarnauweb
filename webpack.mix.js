let mix = require('laravel-mix');

/*
 |--------------------------------------------------------------------------
 | Mix Asset Management
 |--------------------------------------------------------------------------
 |
 | Mix provides a clean, fluent API for defining some Webpack build steps
 | for your Laravel application. By default, we are compiling the Sass
 | file for the application as well as bundling up all the JS files.
 |
 */

 

//mix.js('resources/assets/js/app.js', 'public/js')
mix.scripts(
        [
            'resources/assets/js/vendor/jquery-2.1.3.min.js',
            'resources/assets/js/vendor/modernizr.js',
            'resources/assets/js/vendor/bootstrap.min.js',
            'resources/assets/js/vendor/page-transition.js',
            'resources/assets/js/vendor/imagesloaded.pkgd.min.js.js',
            'resources/assets/js/vendor/validator.js',
            'resources/assets/js/vendor/jquery.shuffle.min.js',
            'resources/assets/js/vendor/masonry.pkgd.min.js',
            'resources/assets/js/vendor/owl.carousel.min.js',
            'resources/assets/js/vendor/jquery.magnific-popup.min.js',
            'resources/assets/js/vendor/jquery.hoverdir.js',
            'resources/assets/js/main.js',
        ],
        'public/js/web.js'
    )
    .sass('resources/assets/sass/app.scss', 'public/css')
        .options({
            postCss: [
                require('postcss-cssnext')({
                    browsers: ['> 5%', 'ie 8']
                }),
                require('postcss-mixins'),
                require('postcss-nested'),
                require('rucksack-css'),
                require('csswring'),
            ]
       });
    ;
