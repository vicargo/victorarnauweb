<!-- This file is used to store sidebar items, starting with Backpack\Base 0.9.0 -->
<li>
    <a href="{{ backpack_url('datos') }}">
        <i class="fa fa-user"></i> <span>Datos Personales</span>
    </a>
</li>
<li class="treeview">
    <a href="#"><i class="fa fa-newspaper-o"></i> <span>Posts</span> <i class="fa fa-angle-left pull-right"></i></a>
    <ul class="treeview-menu">
        <li>
            <a href="{{ url(config('backpack.base.route_prefix', 'admin') . '/articulos') }}">
                <i class="fa fa-newspaper-o"></i> <span>Artículos</span>
            </a>
        </li>
        <li>
            <a href="{{ url(config('backpack.base.route_prefix', 'admin') . '/categorias') }}">
                <i class="fa fa-list"></i> <span>Categorias</span>
            </a>
        </li>
        <li>
            <a href="{{ url(config('backpack.base.route_prefix', 'admin') . '/etiquetas') }}">
                <i class="fa fa-tag"></i> <span>Etiquetas</span>
            </a>
        </li>
    </ul>
</li>
<li>
    <a  href="{{ backpack_url('habilidades') }}">
        <i class="fa fa-bolt"></i> <span>Habilidades</span>
    </a>
</li>
<li>
    <a  href="{{ backpack_url('trabajos') }}">
        <i class="fa fa-css3"></i> <span>Experiencia</span>
    </a>
</li>
<li>
    <a  href="{{ backpack_url('proyectos') }}">
        <i class="fa fa-star"></i> <span>Proyectos</span>
    </a>
</li>
<li>
    <a  href="{{ backpack_url('estudios') }}">
        <i class="fa fa-graduation-cap"></i> <span>Estudios</span>
    </a>
</li>
<li>
    <a  href="{{ backpack_url('aficiones') }}">
        <i class="fa fa-bicycle"></i> <span>Aficiones</span>
    </a>
</li>
<li>
    <a  href="{{ backpack_url('elfinder') }}">
        <i class="fa fa-files-o"></i> <span>File manager</span>
    </a>
</li>