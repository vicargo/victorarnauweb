<!DOCTYPE html>
<html lang="{{ app()->getLocale() }}">
<head>
    <meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1">

    <!-- CSRF Token -->
    <meta name="csrf-token" content="{{ csrf_token() }}">

    <title>@yield('title')</title>


    <link href="{{ asset('css/app.css') }}" rel="stylesheet">
</head>
<body>

    <!-- Loading animation -->
    <div class="preloader">
        <div class="preloader-animation">
            <div class="dot1"></div>
            <div class="dot2"></div>
        </div>
    </div>
    <!-- /Loading animation -->

    <!-- Single Post page -->
    <div id="page" class="page">

        <!-- Header -->
        <header id="site_header" class="header mobile-menu-hide">
            <div class="my-photo">
                <img src="images/datos/{!!$datos[0]->image!!}" alt="image">
                <div class="mask"></div>
            </div>

            <div class="site-title-block">
                <h1 class="site-title">{!! $datos[0]->nombre !!} {!! $datos[0]->apellido !!}</h1>
                <p class="site-description">Desarrollador web</p>
            </div>

            <!-- Navigation & Social buttons -->
            <div class="site-nav">
                <!-- Main menu -->
                <ul id="nav" class="site-main-menu">
                    <!-- About Me Subpage link -->
                    <li>
                        <a class="pt-trigger" href="#home" data-animation="58" data-goto="1">Home</a><!-- href value = data-id without # of .pt-page. Data-goto is the number of section -->
                    </li>
                    <li>
                        <a class="pt-trigger" href="#about_me" data-animation="59" data-goto="2">Sobre mi</a>
                    </li>
                    <li>
                        <a class="pt-trigger" href="#resume" data-animation="60" data-goto="3">Experiencia</a>
                    </li>
                    <li>
                        <a class="pt-trigger" href="#portfolio" data-animation="61" data-goto="4">Portfolio</a>
                    </li>
                    <li>
                        <a class="pt-trigger" href="#blog" data-animation="58" data-goto="5">Blog</a>
                    </li>
                    <li>
                        <a class="pt-trigger" href="#contact" data-animation="59" data-goto="6">Contacto</a>
                    </li>
                </ul>
                <!-- /Main menu -->

                <!-- Social buttons -->
                <ul class="social-links">
                    <li><a class="tip social-button" href="#" title="Twitter"><i class="fa fa-twitter"></i></a></li> 
                    <li><a class="tip social-button" href="#" title="Flickr"><i class="fa fa-flickr"></i></a></li>
                    <li><a class="tip social-button" href="#" title="Github"><i class="fa fa-github"></i></a></li>
                    <li><a class="tip social-button" href="#" title="Gitlab"><i class="fa fa-gitlab"></i></a></li>
                    <!--<li><a class="tip social-button" href="#" title="Youtube"><i class="fa fa-youtube"></i></a></li>-->
                    <!--<li><a class="tip social-button" href="#" title="last.fm"><i class="fa fa-lastfm"></i></a></li>-->
                    <!--<li><a class="tip social-button" href="#" title="Dribbble"><i class="fa fa-dribbble"></i></a></li>-->
                </ul>
                <!-- /Social buttons -->
            </div>
            <!-- Navigation & Social buttons -->
        </header>
        <!-- /Header -->

        <!-- Mobile Header -->
        <div class="mobile-header mobile-visible">
            <div class="mobile-logo-container">
                <div class="mobile-site-title">{!! $datos[0]->nombre !!} {!! $datos[0]->apellido !!}</div>
            </div>

            <a class="menu-toggle mobile-visible">
                <i class="fa fa-bars"></i>
            </a>
        </div>
        <!-- /Mobile Header -->

        @yield('content')

    </div>

    

    
    <!-- Scripts -->
    <script src="{{ asset('js/web.js') }}" defer></script>

    @yield('jquery_ready')
</body>

</html>
