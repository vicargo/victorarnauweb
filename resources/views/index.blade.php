@extends('layouts.web')

@section('title', 'Victor Arnau Web')


@section('content')
  <!-- Main Content -->
    <div id="main" class="site-main">
               
        <!-- Page changer wrapper -->
        <div class="pt-wrapper">
            <!-- Subpages -->
            <div class="subpages">

                <!-- Home Subpage -->
                <section 
                    class="pt-page pt-page-1 section-with-bg section-paddings-0 flex-centered" style="background-image: url(images/datos/{!!$datos[0]->portada!!});" data-id="home">
                    <div class="home-page-block-bg">
                        <div class="mask"></div>
                    </div>
                    <div class="home-page-block">
                        <div class="v-align">
                            <!-- <h2>{!! $datos[0]->nombre !!} {!! $datos[0]->apellido !!}</h2> -->
                            <div id="rotate" class="text-rotate">
                                <div>
                                    <p class="home-page-description">LAMP STACK (Linux/Apache/mysql/php) con LARAVEL y CODEIGNITER</p>
                                </div>
                                <div>
                                    <p class="home-page-description">FRONTEND con sass/postcss, gulp/webpack/parcel.js, jQuery, Animaciones</p>
                                </div>
                            </div>

                            <div class="block end" style="text-align: center">
                                <ul class="info-list">
                                    <!-- <li><span class="title">Edad</span><span class="value">{!! $datos[0]->edad !!}</span></li> -->
                                    <li><span class="title">e-mail</span><span class="value">{!! $datos[0]->email !!}</span></li>
                                    <li><span class="title">Teléfono</span><span class="value">{!! $datos[0]->telefono !!}</span></li>
                                </ul>
                            </div>
                        </div>
                    </div>
                </section>
                <!-- End of Home Subpage -->

                <!-- About Me Subpage -->
                <section class="pt-page pt-page-2" data-id="about_me">
                    <div class="section-title-block">
                        <h2 class="section-title">Sobre mi</h2>
                        <h5 class="section-description">Frontend camino a Fullstack</h5>
                    </div>

                    <div class="row">
                        <div class="col-sm-6 col-md-6 subpage-block">
                            <div class="general-info">

                                <div class="block-title">
                                    <h3>Conociéndome un poco más</h3>
                                </div>
                                <p>Víctor Arnau Gómez, Barcelona(1986), padre de una nena de 1 año y medio (muy bonita, por cierto) y marido de una mujer maravillosa. Programador web con 3 años de experiencia, 2 como Frontend y 1 como Fullstack.</p>
                                <p>Entusiasta de la tecnología, interesado en la blockchain y las criptomonedas y si, tambien veo Juego de Tronos.</p>
                            </div>
                        </div>

                        <div class="col-sm-6 col-md-6 subpage-block">
                            <div class="block-title">
                                <h3>Testimonios</h3>
                            </div>
                        <div class="testimonials owl-carousel">
                            <!-- Testimonial 2 -->
                            <div class="testimonial-item">
                                <!-- Testimonial Content -->
                                <div class="testimonial-content">
                                    <div class="testimonial-text">
                                        <p>"La primera vez que vi la web quede sorprendido de como había captado mi idea tan rápido"</p>
                                    </div>
                                </div>            
                                <!-- /Testimonial Content -->  
                                <!-- Testimonial Author -->
                                <div class="testimonial-credits">
                                    <!-- Picture -->
                                    <div class="testimonial-picture">
                                        <img src="images/testimonials/testimonila_photo_2.png" alt="">
                                    </div>              
                                    <!-- /Picture -->
                                    <!-- Testimonial author information -->
                                    <div class="testimonial-author-info">
                                        <p class="testimonial-author">Raul Buil</p>
                                        <p class="testimonial-firm">Building</p>
                                    </div>
                                </div>
                                <!-- /Testimonial author information -->               
                            </div>
                            <!-- End of Testimonial 2 -->

                            <!-- Testimonial 2 -->
                            <div class="testimonial-item">
                                <!-- Testimonial Content -->
                                <div class="testimonial-content">
                                    <div class="testimonial-text">
                                        <p>"Tenia muchas dudas de como afrontar mi proyecto personal, después de hablar 5 minutos con Víctor, lo tengo clarísimo."</p>
                                    </div>
                                </div>            
                                <!-- /Testimonial Content -->  
                                <!-- Testimonial Author -->
                                <div class="testimonial-credits">
                                    <!-- Picture -->
                                    <div class="testimonial-picture">
                                        <img src="images/testimonials/testimonila_photo_2.png" alt="">
                                    </div>              
                                    <!-- /Picture -->
                                    <!-- Testimonial author information -->
                                    <div class="testimonial-author-info">
                                        <p class="testimonial-author">Daniel Sanchez</p>
                                        <p class="testimonial-firm">Los Pecas</p>
                                    </div>
                                </div>
                                <!-- /Testimonial author information -->               
                            </div>
                            <!-- End of Testimonial 2 -->

                        </div>
                    </div>
                </div>

            
                <!-- Fun facts block -->
                <div class="block-title">
                    <h3>Fun Facts</h3>
                </div>

                <div class="row">
                    <div class="col-sm-6 col-md-3 subpage-block">
                        <div class="fun-fact-block gray-bg"> 
                            <i class="fa fa-calendar font-size-64"></i>
                            <h4>Años de experiencia</h4>
                            <span class="fun-value">3</span>
                        </div>
                    </div>

                    <div class="col-sm-6 col-md-3 subpage-block">
                        <div class="fun-fact-block"> 
                            <i class="fa fa-code font-size-64"></i>
                            <h4>Lineas de código</h4>
                            <span class="fun-value">10.000</span>
                        </div>
                    </div>

                    <div class="col-sm-6 col-md-3 subpage-block">
                        <div class="fun-fact-block gray-bg"> 
                            <i class="fa fa-star-o font-size-64"></i>
                            <h4>Proyectos publicados</h4>
                            <span class="fun-value">80</span>
                        </div>
                    </div>

                    <div class="col-sm-6 col-md-3 subpage-block">
                        <div class="fun-fact-block"> 
                            <i class="fa fa-coffee font-size-64"></i>
                            <h4>Tazas de café al día</h4>
                            <span class="fun-value">3</span>
                        </div>
                    </div>
                </div>
                <!-- End of Fun fucts block -->
                </section>
                <!-- End of About Me Subpage -->

            <!-- Resume Subpage -->
            <section class="pt-page pt-page-3" data-id="resume">
                <div class="section-title-block">
                    <h2 class="section-title">Experiencia</h2>
                    <h5 class="section-description">3 años de experiencia</h5>
                 </div>

                <div class="row">
                    <div class="col-sm-6 col-md-4 subpage-block">
                        <div class="block-title">
                            <h3>Estudios</h3>
                        </div>
                        <div class="timeline">
                            <!-- Single event -->
                            <div class="timeline-event te-primary">
                                <h5 class="event-date">2017</h5>
                                <h4 class="event-name">Cursos de últimas tecnologias Frontend</h4>
                                <span class="event-description">Plazi</span>
                                <p>Lote de 3 cursos online, curso básico de vue.js, postcss y animaciones css</p>
                            </div>
                            <!-- Single event -->
                            <div class="timeline-event te-primary">
                                <h5 class="event-date">2016</h5>
                                <h4 class="event-name">Cursos de Responsive Design</h4>
                                <span class="event-description">Platzi</span>
                                <p>Responsive design con y sin frameworks, flexbox, media queries, patrones de diseño, mobile-first</p>
                            </div>
                            <!-- Single event -->
                            <div class="timeline-event te-primary">
                                <h5 class="event-date">2014</h5>
                                <h4 class="event-name">CFGS Desarrollo de aplicaciones web</h4>
                                <span class="event-description">IES Joan Brossa</span>
                                <p>Ciclo Formativo de Grado Superior (Formación oficial por el Departament d'enseyament de la Generalitat de Catalunya)</p>
                            </div>
                        </div>
                    </div>

                    <div class="col-sm-6 col-md-4 subpage-block">
                        <div class="block-title">
                            <h3>Experiencia</h3>
                        </div>
                        <div class="timeline">
                            <!-- Single event -->
                            <div class="timeline-event te-primary">
                                <h5 class="event-date">Enero 2018 - Abril 2018</h5>
                                <h4 class="event-name">Fullstack-developer</h4>
                                <span class="event-description">Bymotto</span>
                                <p>Mantenimiento y nuevas features en proyectos hechos con Laravel y CodeIgniter con MySQL. Jquery, sass, gulp, grunt, bootstrap 3 y 4 en el frontend. </p>
                            </div>
                            <!-- Single event -->
                            <div class="timeline-event te-primary">
                                <h5 class="event-date">Junio 2015 - Enero 2018</h5>
                                <h4 class="event-name">Frontend-developer</h4>
                                <span class="event-description">Microlab Hard</span>
                                <p>Diseño y desarrollo de páginas web con HTML5, CSS3, javascript, php. Gulp como package builder y Git como control de versiones.</p>
                            </div>
                            <!-- Single event -->
                            <div class="timeline-event te-primary">
                                <h5 class="event-date">Septiembre 2014 - Noviembre 2015</h5>
                                <h4 class="event-name">Junior Fullstack-developer</h4>
                                <span class="event-description">eGov Digital</span>
                                <p>Mantenimiento y agregar nuevas funcionalidades, php/mysql. Primeros contactos con API's (google maps)</p>
                            </div>
                        </div>
                    </div>

                    <div class="col-sm-6 col-md-4 subpage-block">
                        <div class="block-title">
                            <h3>Habilidades Frontend</h3>
                        </div>
                        <div class="skills-info">
                            <h4>Responsive design</h4>                               
                            <div class="skill-container">
                                <div class="skill-percentage skill-1"></div>
                            </div>

                            <h4>HTML5</h4>
                            <div class="skill-container">
                                <div class="skill-percentage skill-2"></div>
                            </div>

                            <h4>CSS3</h4>
                            <div class="skill-container">
                                <div class="skill-percentage skill-8"></div>
                            </div>

                            <h4>Javascript/jQuery</h4>
                            <div class="skill-container">
                                <div class="skill-percentage skill-3"></div>
                            </div> 
                        </div>

                        <div class="block-title">
                            <h3>Habilidades Backend/DevOps</h3>
                        </div>
                        <div class="skills-info">
                            <h4>PHP</h4>                               
                            <div class="skill-container">
                                <div class="skill-percentage skill-4"></div>
                            </div>

                            <h4>Laravel</h4>
                            <div class="skill-container">
                                <div class="skill-percentage skill-5"></div>
                            </div>

                            <h4>Code Igniter</h4>
                            <div class="skill-container">
                                <div class="skill-percentage skill-6"></div>
                            </div>

                            <h4>Digital Ocen (DevOps)</h4>
                            <div class="skill-container">
                                <div class="skill-percentage skill-7"></div>
                            </div>
                        </div>
                    </div>
                </div>
                <div class="row">
                    <div class="col-sm-12 col-md-12">
                        <div class="download-cv-block">
                            <a class="button" target="_blank" href="#">Descargar CV</a>
                        </div>
                    </div>
                </div>
            </section>
            <!-- End Resume Subpage -->


            <!-- Portfolio Subpage -->
            <section class="pt-page pt-page-4" data-id="portfolio">
              <div class="section-title-block">
                <h2 class="section-title">Portfolio</h2>
                <h5 class="section-description">Algunos de mis trabajos</h5>
              </div>

              <!-- Portfolio Content -->
              <div class="portfolio-content">
                            
                <!-- Portfolio filter -->
                <ul id="portfolio_filters" class="portfolio-filters">
                  <li class="active">
                    <a class="filter btn btn-sm btn-link active" data-group="all">All</a>
                  </li>
                  <li>
                    <a class="filter btn btn-sm btn-link" data-group="media">Laravel</a>
                  </li>
                  <li>
                    <a class="filter btn btn-sm btn-link" data-group="ci">Code igniter</a>
                  </li>
                  <li>
                    <a class="filter btn btn-sm btn-link" data-group="video">Php</a>
                  </li>
                  <li>
                    <a class="filter btn btn-sm btn-link" data-group="wp">Wordpress</a>
                  </li>
                </ul>
                <!-- End of Portfolio filter -->

                <!-- Portfolio Grid -->
                <div id="portfolio_grid" class="portfolio-grid portfolio-masonry masonry-grid-3">

                  <!-- Portfolio Item 1 -->
                  <figure class="item" data-groups='["all", "media"]'>
                    <a class="ajax-page-load" href="portfolio-1.html">
                      <img src="images/portfolio/1.jpg" alt="">
                      <div>
                        <h5 class="name">Project Name</h5>
                        <small>Media</small>
                        <i class="fa fa-file-text-o"></i>
                      </div>
                    </a>
                  </figure>
                  <!-- /Portfolio Item 1 -->

                  <!-- Portfolio Item 2 -->
                  <figure class="item" data-groups='["all", "video"]'>
                    <a href="https://player.vimeo.com/video/97102654?autoplay=1" title="Praesent Dolor Ex" class="lightbox mfp-iframe">
                      <img src="images/portfolio/2.jpg" alt="">
                      <div>
                        <h5 class="name">Project Name</h5>
                        <small>Video</small>
                        <i class="fa fa-video-camera"></i>
                      </div>
                    </a>
                  </figure>
                  <!-- /Portfolio Item 2 -->

                  <!-- Portfolio Item 3 -->
                  <figure class="item" data-groups='["all","illustration"]'>
                    <a href="images/portfolio/3.jpg" class="lightbox" title="Duis Eu Eros Viverra">
                      <img src="images/portfolio/3.jpg" alt="">
                      <div>
                        <h5 class="name">Project Name</h5>
                        <small>Illustration</small>
                        <i class="fa fa-image"></i>
                      </div>
                    </a>
                  </figure>
                  <!-- /Portfolio Item 3 -->

                  <!-- Portfolio Item 4 -->
                  <figure class="item" data-groups='["all", "media"]'>
                    <a class="ajax-page-load" href="portfolio-1.html">
                      <img src="images/portfolio/4.jpg" alt="">
                      <div>
                        <h5 class="name">Project Name</h5>
                        <small>Media</small>
                        <i class="fa fa-file-text-o"></i>
                      </div>
                    </a>
                  </figure>
                  <!-- /Portfolio Item 4 -->

                  <!-- Portfolio Item 5 -->
                  <figure class="item" data-groups='["all", "ci"]'>
                    <a href="images/portfolio/5.jpg" class="lightbox" title="Aliquam Condimentum Magna Rhoncus">
                      <img src="images/portfolio/5.jpg" alt="">
                      <div>
                        <h5 class="name">Project Name</h5>
                        <small>Illustration</small>
                        <i class="fa fa-image"></i>
                      </div>
                    </a>
                  </figure>
                  <!-- /Portfolio Item 5 -->

                  <!-- Portfolio Item 6 -->
                  <figure class="item" data-groups='["all", "media"]'>
                    <a class="ajax-page-load" href="portfolio-1.html">
                      <img src="images/portfolio/6.jpg" alt="">
                      <div>
                        <h5 class="name">Project Name</h5>
                        <small>Media</small>
                        <i class="fa fa-file-text-o"></i>
                      </div>
                    </a>
                  </figure>
                  <!-- /Portfolio Item 6 -->

                  <!-- Portfolio Item 7 -->
                  <figure class="item" data-groups='["all", "video"]'>
                    <a href="https://player.vimeo.com/video/97102654?autoplay=1" title="Nulla Facilisi" class="lightbox mfp-iframe">
                      <img src="images/portfolio/7.jpg" alt="">
                      <div>
                        <h5 class="name">Project Name</h5>
                        <small>Video</small>
                        <i class="fa fa-video-camera"></i>
                      </div>
                    </a>
                  </figure>
                  <!-- /Portfolio Item 7 -->

                  <!-- Portfolio Item 8 -->
                  <figure class="item" data-groups='["all",  "wp"]'>
                    <a class="ajax-page-load" href="portfolio-1.html">
                      <img src="images/portfolio/8.jpg" alt="">
                      <div>
                        <h5 class="name">Project Name</h5>
                        <small>Media</small>
                        <i class="fa fa-file-text-o"></i>
                      </div>
                    </a>
                  </figure>
                  <!-- /Portfolio Item 8 -->

                  <!-- Portfolio Item 9 -->
                  <figure class="item" data-groups='["all","illustration"]'>
                    <a href="images/portfolio/9.jpg" class="lightbox" title="Mauris Neque Dolor">
                      <img src="images/portfolio/9.jpg" alt="">
                      <div>
                        <h5 class="name">Project Name</h5>
                        <small>Illustration</small>
                        <i class="fa fa-image"></i>
                      </div>
                    </a>
                  </figure>
                  <!-- /Portfolio Item 9 -->

                  <!-- Portfolio Item 10 -->
                  <figure class="item" data-groups='["all", "video"]'>
                    <a href="https://player.vimeo.com/video/97102654?autoplay=1" title="Donec Lectus Arcu" class="lightbox mfp-iframe">
                      <img src="images/portfolio/10.jpg" alt="">
                      <div>
                        <h5 class="name">Project Name</h5>
                        <small>Video</small>
                        <i class="fa fa-video-camera"></i>
                      </div>
                    </a>
                  </figure>
                  <!-- /Portfolio Item 10 -->

                  <!-- Portfolio Item 11 -->
                  <figure class="item" data-groups='["all","illustration"]'>
                    <a href="images/portfolio/11.jpg" class="lightbox" title="Duis Eu Eros Viverra">
                      <img src="images/portfolio/11.jpg" alt="">
                      <div>
                        <h5 class="name">Project Name</h5>
                        <small>Illustration</small>
                        <i class="fa fa-image"></i>
                      </div>
                    </a>
                  </figure>
                  <!-- /Portfolio Item 11 -->

                  <!-- Portfolio Item 12 -->
                  <figure class="item" data-groups='["all","media"]'>
                    <a class="ajax-page-load" href="portfolio-1.html">
                      <img src="images/portfolio/12.jpg" alt="">
                      <div>
                        <h5 class="name">Project Name</h5>
                        <small>Media</small>
                        <i class="fa fa-file-text-o"></i>
                      </div>
                    </a>
                  </figure>
                  <!-- /Portfolio Item 12 -->
                </div>
                <!-- /Portfolio Grid -->

              </div>
              <!-- /Portfolio Content -->

            </section>
            <!-- /Portfolio Subpage -->

            <!-- Blog Subpage -->
            <section class="pt-page pt-page-5" data-id="blog">
              <div class="section-title-block">
                <h2 class="section-title">Blog</h2>
                <h5 class="section-description">¿Como hice aquello?</h5>
              </div>
              <div class="blog-masonry">
                <!-- Blog Post 1 -->
                <div class="item">
                  <div class="blog-card">
                    <div class="media-block">
                      <a href="blog-post-1.html">
                        <img class="post-image img-responsive" src="images/blog/blog_post_1.jpg" alt="blog-post-1" />
                        <div class="mask"></div>
                        <div class="post-date"><span class="day">6</span><span class="month">Dec</span><!--<span class="year">2016</span>--></div>
                      </a>
                    </div>
                    <div class="post-info">
                      <ul class="category">
                        <li><a href="#">Travel</a></li>
                      </ul>
                      <a href="blog-post-1.html"><h4 class="blog-item-title">Bootstrap is the Most Popular Framework</h4></a>
                    </div>
                  </div>
                </div>
                <!-- End of Blog Post 1 -->

                <!-- Blog Post 2 -->
                <div class="item">
                  <div class="blog-card">
                    <div class="media-block">
                      <a href="blog-post-1.html">
                        <img class="post-image img-responsive" src="images/blog/blog_post_2.jpg" alt="blog-post-2" />
                        <div class="mask"></div>
                        <div class="post-date"><span class="day">3</span><span class="month">Nov</span><!--<span class="year">2016</span>--></div>
                      </a>
                    </div>
                    <div class="post-info">
                      <ul class="category">
                        <li><a href="#">jQuery</a></li>
                      </ul>
                      <a href="blog-post-1.html"><h4 class="blog-item-title">One Framework, Every Device</h4></a>
                    </div>
                  </div>
                </div>
                <!-- End of Blog Post 2 -->

              </div>
            </section>
            <!-- End Blog Subpage -->

            <!-- Contact Subpage -->
            <section class="pt-page pt-page-6" data-id="contact">
              <div class="section-title-block">
                <h2 class="section-title">Contacto</h2>
                <h5 class="section-description">¿Quieres saber más?</h5>
              </div>

              <div class="row">
                <div class="col-sm-6 col-md-6 subpage-block">
                  <div class="block-title">
                    <h3>Contacta conmigo</h3>
                  </div>
                  <p>Puedes contactar conmigo si quieres saber más de algún proyecto en el que haya participado, o para aclarar cualquier duda, o por si quieres contar conmigo para tu proyecto.  </p>
                  <div class="contact-info-block">
                    <div class="ci-icon">
                      <i class="pe-7s-icon pe-7s-map-marker"></i>
                    </div>
                    <div class="ci-text">
                      <h5>Barcelona, Catalunya (España)</h5>
                    </div>
                  </div>
                  <div class="contact-info-block">
                    <div class="ci-icon">
                      <i class="pe-7s-icon pe-7s-mail"></i>
                    </div>
                    <div class="ci-text">
                      <h5>{!! $datos[0]->email !!}</h5>
                    </div>
                  </div>
                  <div class="contact-info-block">
                    <div class="ci-icon">
                      <i class="pe-7s-icon pe-7s-call"></i>
                    </div>
                    <div class="ci-text">
                      <h5>{!! $datos[0]->telefono !!}</h5>
                    </div>
                  </div>
                  <div class="contact-info-block">
                    <div class="ci-icon">
                      <i class="pe-7s-icon pe-7s-check"></i>
                    </div>
                    <div class="ci-text">
                      <h5>Freelance disponible</h5>
                    </div>
                  </div>
                </div>

                <div class="col-sm-6 col-md-6 subpage-block">
                  <div class="block-title">
                    <h3>Contact Form</h3>
                  </div>
                  <form id="contact-form" method="post" action="contact_form/contact_form.php">

                    <div class="messages"></div>

                    <div class="controls">
                      <div class="form-group">
                          <input id="form_name" type="text" name="name" class="form-control" placeholder="Nombre" required="required" data-error="Name is required.">
                          <div class="form-control-border"></div>
                          <i class="form-control-icon fa fa-user"></i>
                          <div class="help-block with-errors"></div>
                      </div>

                      <div class="form-group">
                          <input id="form_email" type="email" name="email" class="form-control" placeholder="Email" required="required" data-error="Valid email is required.">
                          <div class="form-control-border"></div>
                          <i class="form-control-icon fa fa-envelope"></i>
                          <div class="help-block with-errors"></div>
                      </div>

                      <div class="form-group">
                          <textarea id="form_message" name="message" class="form-control" placeholder="Mensaje para mi" rows="4" required="required" data-error="Please, leave me a message."></textarea>
                          <div class="form-control-border"></div>
                          <i class="form-control-icon fa fa-comment"></i>
                          <div class="help-block with-errors"></div>
                      </div>

                      <!-- <div class="g-recaptcha" data-sitekey="6LdqmCAUAAAAAMMNEZvn6g4W5e0or2sZmAVpxVqI" data-callback="correctCaptcha"></div> -->

                      <input type="submit" class="button btn-send" value="Enviar mensaje">
                    </div>
                  </form>
                </div>
              </div>
            </section>
            <!-- End Contact Subpage -->

          </div>
        </div>
        <!-- /Page changer wrapper -->
      </div>
      <!-- /Main Content -->
@endsection
