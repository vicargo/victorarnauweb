@extends('layouts.web')

@section('header')
<header id="site_header" class="header mobile-menu-hide">
    <div class="my-photo">
        <img src="images/my_photo.png" alt="image">
        <div class="mask"></div>
    </div>

    <div class="site-title-block">
        <h1 class="site-title">Alex Smith</h1>
        <p class="site-description">Web Designer</p>
    </div>

    <!-- Navigation & Social buttons -->
    <div class="site-nav">
        <!-- Main menu -->
        <ul id="nav" class="site-main-menu">
            <!-- About Me Subpage link -->
            <li>
                <a class="pt-trigger" href="index.html#home">Home</a><!-- href value = data-id without # of .pt-page. Data-goto is the number of section -->
            </li>
            <!-- /About Me Subpage link -->
            <!-- About Me Subpage link -->
            <li>
                <a class="pt-trigger" href="index.html#about_me">About me</a>
            </li>
            <!-- /About Me Subpage link -->
            <li>
                <a class="pt-trigger" href="index.html#resume">Resume</a>
            </li>
            <li>
                <a class="pt-trigger" href="index.html#portfolio">Portfolio</a>
            </li>
            <li class="active">
                <a class="pt-trigger" href="index.html#blog">Blog</a>
            </li>
            <li>
                <a class="pt-trigger" href="index.html#contact">Contact</a>
            </li>
        </ul>
        <!-- /Main menu -->

        <!-- Social buttons -->
        <ul class="social-links">
        <li><a class="tip social-button" href="#" title="Twitter"><i class="fa fa-twitter"></i></a></li> <!-- Full list of social icons: http://fontawesome.io/icons/#brand -->
        <li><a class="tip social-button" href="#" title="Facebook"><i class="fa fa-facebook"></i></a></li>
        <li><a class="tip social-button" href="#" title="Google Plus"><i class="fa fa-google-plus"></i></a></li>
        <!--<li><a class="tip social-button" href="#" title="Youtube"><i class="fa fa-youtube"></i></a></li>-->
        <!--<li><a class="tip social-button" href="#" title="last.fm"><i class="fa fa-lastfm"></i></a></li>-->
        <!--<li><a class="tip social-button" href="#" title="Dribbble"><i class="fa fa-dribbble"></i></a></li>-->
        </ul>
        <!-- /Social buttons -->
    </div>
    <!-- Navigation & Social buttons -->
</header>

<!-- Mobile Header -->
<div class="mobile-header mobile-visible">
    <div class="mobile-logo-container">
        <div class="mobile-site-title">Alex Smith</div>
    </div>

    <a class="menu-toggle mobile-visible">
        <i class="fa fa-bars"></i>
    </a>
</div>
<!-- /Mobile Header -->
@endsection