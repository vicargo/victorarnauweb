<?php

// --------------------------
// Custom Backpack Routes
// --------------------------
// This route file is loaded automatically by Backpack\Base.
// Routes you generate using Backpack\Generators will be placed here.

Route::group([
    'prefix'     => config('backpack.base.route_prefix', 'admin'),
    'middleware' => ['web', config('backpack.base.middleware_key', 'admin')],
    'namespace'  => 'App\Http\Controllers\Admin',
], function () { // custom admin routes
    //CRUD::resource('persona', 'PersonaCrudController');
    CRUD::resource('habilidades', 'HabilidadCrudController');
    CRUD::resource('trabajos', 'TrabajoCrudController');
    CRUD::resource('proyectos', 'ProyectoCrudController');
    CRUD::resource('estudios', 'EstudioCrudController');
    CRUD::resource('paginas', 'PaginaCrudController');
    CRUD::resource('aficiones', 'AficionCrudController');
    CRUD::resource('datos', 'DatoCrudController');


    //POSTS
    CRUD::resource('articulos', 'ArticleCrudController');
    CRUD::resource('categorias', 'CategoryCrudController');
    CRUD::resource('etiquetas', 'TagCrudController');
}); // this should be the absolute last line of this file
