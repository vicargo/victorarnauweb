<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
//use Illuminate\Support\Facades\Auth;

use App\Models\Dato;
use App\Models\Habilidad;


class HomeController extends Controller
{
    /**
     * Show the application dashboard.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {   
        // Lista de categorias
        $datos = Dato::where('id', 1)->get();
        $habiliaddes = Habilidad::all();


        return view('index',[
            'datos' => $datos, 
            'habilidades' => $habiliaddes
        ]);
    }
}
