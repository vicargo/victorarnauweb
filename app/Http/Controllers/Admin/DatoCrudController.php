<?php

namespace App\Http\Controllers\Admin;

use Backpack\CRUD\app\Http\Controllers\CrudController;

// VALIDATION: change the requests to match your own file names if you need form validation
use App\Http\Requests\DatoRequest as StoreRequest;
use App\Http\Requests\DatoRequest as UpdateRequest;

class DatoCrudController extends CrudController
{
    public function setup()
    {

        /*
        |--------------------------------------------------------------------------
        | BASIC CRUD INFORMATION
        |--------------------------------------------------------------------------
        */
        $this->crud->setModel('App\Models\Dato');
        $this->crud->setRoute(config('backpack.base.route_prefix') . '/datos');
        $this->crud->setEntityNameStrings('dato', 'datos');

        /*
        |--------------------------------------------------------------------------
        | BASIC CRUD INFORMATION
        |--------------------------------------------------------------------------
        */

        //$this->crud->setFromDb();
        $this->crud->addColumn([
            'name' => 'nombre',
            'label' => 'Nombre'
        ]);
        $this->crud->addColumn([
            'name' => 'apellido',
            'label' => 'Apellido'
        ]);
        $this->crud->addColumn([
            'name' => 'image',
            'label' => 'Imagen ficha',
            'type' => 'image',
            'height' => '100px',
            'width' => '100px',
            'prefix' => 'images/datos/',
        ]);
        $this->crud->addColumn([
            'name' => 'portada',
            'label' => 'Imagen portada',
            'type' => 'image',
            'height' => '100px',
            'prefix' => 'images/datos/',
        ]);

       

        // ------ CRUD FIELDS
        $this->crud->addField([
            'name' => 'nombre',
            'label' => 'Nombre',
            'type' => 'text',
        ]);
        $this->crud->addField([
            'name' => 'apellido',
            'label' => 'Apellido',
            'type' => 'text',
        ]);
        $this->crud->addField([
            'name' => 'edad',
            'label' => 'edad',
            'type' => 'number',
        ]);
        $this->crud->addField([
            'name' => 'telefono',
            'label' => 'Teléfono',
            'type' => 'text',
        ]);
        $this->crud->addField([
            'name' => 'email',
            'label' => 'Email',
            'type' => 'email',
        ]);
        $this->crud->addField([
            'name' => 'titulo_desc',
            'label' => 'Título descripción',
            'type' => 'text',
        ]);
        $this->crud->addField([
            'name' => 'descripcion',
            'label' => 'Descripción',
            'type' => 'wysiwyg',
        ]);
        $this->crud->addField([
            'name' => 'frase',
            'label' => 'Frase',
            'type' => 'text',
        ]);
        $this->crud->addField([
            'name' => 'ff_icon_1',
            'label' => 'Fun Fact Icon 1',
            'type' => 'icon_picker',
            'iconset' => 'fontawesome'
        ]);
        $this->crud->addField([
            'name' => 'ff_text_1',
            'label' => 'Fun Fact Text 1',
            'type' => 'text',
        ]);
        $this->crud->addField([
            'name' => 'ff_number_1',
            'label' => 'Fun Fact Number 1',
            'type' => 'number',
        ]);
        $this->crud->addField([
            'name' => 'Fun Fact Icon 2',
            'label' => 'ff_icon_2',
            'type' => 'icon_picker',
            'iconset' => 'fontawesome'
        ]);
        $this->crud->addField([
            'name' => 'ff_text_2',
            'label' => 'Fun Fact Text 2',
            'type' => 'text',
        ]);
        $this->crud->addField([
            'name' => 'ff_number_2',
            'label' => 'Fun Fact Number 2',
            'type' => 'number',
        ]);
        $this->crud->addField([
            'name' => 'Fun Fact Icon 3',
            'label' => 'ff_icon_3',
            'type' => 'icon_picker',
            'iconset' => 'fontawesome'
        ]);
        $this->crud->addField([
            'name' => 'ff_text_3',
            'label' => 'Fun Fact Text 3',
            'type' => 'text',
        ]);
        $this->crud->addField([
            'name' => 'ff_number_3',
            'label' => 'Fun Fact Number 3',
            'type' => 'number',
        ]);
        $this->crud->addField([
            'name' => 'Fun Fact Icon 4',
            'label' => 'ff_icon_4',
            'type' => 'icon_picker',
            'iconset' => 'fontawesome'
        ]);
        $this->crud->addField([
            'name' => 'ff_text_4',
            'label' => 'Fun Fact Text 4',
            'type' => 'text',
        ]);
        $this->crud->addField([
            'name' => 'ff_number_4',
            'label' => 'Fun Fact Number 4',
            'type' => 'number',
        ]);
        $this->crud->addField([
            'name' => 'image',
            'label' => 'Imagen de perfil',
            'type' => 'image',
            'upload' => true,
            'crop' => true,
            'prefix' => 'images/datos/',
            'aspect_ratio' => 1,
        ]);
        $this->crud->addField([
            'name' => 'portada',
            'label' => 'Imagen de portada',
            'type' => 'image',
            'upload' => true,
            'crop' => true,
            'prefix' => 'images/datos/',
            'aspect_ratio' => 2,
        ]);
        $this->crud->addField([
            'name' => 'facebook',
            'label' => 'Facebook',
            'type' => 'text',
        ]);
        $this->crud->addField([
            'name' => 'twitter',
            'label' => 'Twitter',
            'type' => 'text',
        ]);
        $this->crud->addField([
            'name' => 'instragram',
            'label' => 'Instragram',
            'type' => 'text',
        ]);
        $this->crud->addField([
            'name' => 'github',
            'label' => 'Github',
            'type' => 'text',
        ]);
        $this->crud->addField([
            'name' => 'gitlab',
            'label' => 'Gitlab',
            'type' => 'text',
        ]);
        $this->crud->addField([
            'name' => 'blog',
            'label' => 'Blog',
            'type' => 'text',
        ]);
        // ------ CRUD FIELDS
        // $this->crud->addField($options, 'update/create/both');
        // $this->crud->addFields($array_of_arrays, 'update/create/both');
        // $this->crud->removeField('name', 'update/create/both');
        // $this->crud->removeFields($array_of_names, 'update/create/both');

        // ------ CRUD COLUMNS
        // $this->crud->addColumn(); // add a single column, at the end of the stack
        // $this->crud->addColumns(); // add multiple columns, at the end of the stack
        // $this->crud->removeColumn('column_name'); // remove a column from the stack
        // $this->crud->removeColumns(['column_name_1', 'column_name_2']); // remove an array of columns from the stack
        // $this->crud->setColumnDetails('column_name', ['attribute' => 'value']); // adjusts the properties of the passed in column (by name)
        // $this->crud->setColumnsDetails(['column_1', 'column_2'], ['attribute' => 'value']);

        // ------ CRUD BUTTONS
        // possible positions: 'beginning' and 'end'; defaults to 'beginning' for the 'line' stack, 'end' for the others;
        // $this->crud->addButton($stack, $name, $type, $content, $position); // add a button; possible types are: view, model_function
        // $this->crud->addButtonFromModelFunction($stack, $name, $model_function_name, $position); // add a button whose HTML is returned by a method in the CRUD model
        // $this->crud->addButtonFromView($stack, $name, $view, $position); // add a button whose HTML is in a view placed at resources\views\vendor\backpack\crud\buttons
        // $this->crud->removeButton($name);
        // $this->crud->removeButtonFromStack($name, $stack);
        // $this->crud->removeAllButtons();
        // $this->crud->removeAllButtonsFromStack('line');

        // ------ CRUD ACCESS
        // $this->crud->allowAccess(['list', 'create', 'update', 'reorder', 'delete']);
        // $this->crud->denyAccess(['list', 'create', 'update', 'reorder', 'delete']);

        // ------ CRUD REORDER
        // $this->crud->enableReorder('label_name', MAX_TREE_LEVEL);
        // NOTE: you also need to do allow access to the right users: $this->crud->allowAccess('reorder');

        // ------ CRUD DETAILS ROW
        // $this->crud->enableDetailsRow();
        // NOTE: you also need to do allow access to the right users: $this->crud->allowAccess('details_row');
        // NOTE: you also need to do overwrite the showDetailsRow($id) method in your EntityCrudController to show whatever you'd like in the details row OR overwrite the views/backpack/crud/details_row.blade.php

        // ------ REVISIONS
        // You also need to use \Venturecraft\Revisionable\RevisionableTrait;
        // Please check out: https://laravel-backpack.readme.io/docs/crud#revisions
        // $this->crud->allowAccess('revisions');

        // ------ AJAX TABLE VIEW
        // Please note the drawbacks of this though:
        // - 1-n and n-n columns are not searchable
        // - date and datetime columns won't be sortable anymore
        // $this->crud->enableAjaxTable();

        // ------ DATATABLE EXPORT BUTTONS
        // Show export to PDF, CSV, XLS and Print buttons on the table view.
        // Does not work well with AJAX datatables.
        // $this->crud->enableExportButtons();

        // ------ ADVANCED QUERIES
        // $this->crud->addClause('active');
        // $this->crud->addClause('type', 'car');
        // $this->crud->addClause('where', 'name', '==', 'car');
        // $this->crud->addClause('whereName', 'car');
        // $this->crud->addClause('whereHas', 'posts', function($query) {
        //     $query->activePosts();
        // });
        // $this->crud->addClause('withoutGlobalScopes');
        // $this->crud->addClause('withoutGlobalScope', VisibleScope::class);
        // $this->crud->with(); // eager load relationships
        // $this->crud->orderBy();
        // $this->crud->groupBy();
        // $this->crud->limit();
    }

    public function store(StoreRequest $request)
    {
        // your additional operations before save here
        $redirect_location = parent::storeCrud($request);
        // your additional operations after save here
        // use $this->data['entry'] or $this->crud->entry
        return $redirect_location;
    }

    public function update(UpdateRequest $request)
    {
        // your additional operations before save here
        $redirect_location = parent::updateCrud($request);
        // your additional operations after save here
        // use $this->data['entry'] or $this->crud->entry
        return $redirect_location;
    }
}
