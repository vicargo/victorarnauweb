<?php

use Illuminate\Database\Seeder;
use App\User;

// composer require laracasts/testdummy
use Laracasts\TestDummy\Factory as TestDummy;

class UsersTableSeeder extends Seeder
{
    public function run()
    {
        // TestDummy::times(20)->create('App\Post');
        User::create([
            'nombre' => 'Víctor',
            'email' => 'vicargoapp@gmail.com',
            'password' => bcrypt('43561544g'),
        ]);
    }
}
