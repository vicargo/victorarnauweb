<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class AddFieldsToHabilidadesTable extends Migration
{
    /**
     * Run the migrations.
     * 
     * Esta migración la hacemos debido a como Laravel ejecuta las migraciones cuando tenemos relaciones muchos a 
     * muchoos, las habilidades tienen relación con proyectos
     *
     * @return void
     */
    public function up()
    {
        Schema::table('habilidades', function(Blueprint $table) {
            $table->string('nombre');
            $table->text('descripcion');
            $table->binary('image')->nullable();
            $table->enum('status', ['PUBLICADO', 'INACTIVO'])->default('PUBLICADO');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::table('estudios', function(Blueprint $table) {
            $table->dropColumn('nombre');
            $table->dropColumn('descripcion');
            $table->dropColumn('image');
            $table->dropColumn('status');
        });
    }
}
