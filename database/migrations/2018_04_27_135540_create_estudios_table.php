<?php

use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateEstudiosTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('estudios', function (Blueprint $table) {
            $table->increments('id');
            $table->string('nombre');
            $table->text('descripcion')->nullable();
            $table->string('lugar')->nullable();
            $table->date('fecha_ini');
            $table->date('fecha_fin')->nullable();
            $table->boolean('is_online')->nullable();
            $table->string('link_pdf')->nullable();
            $table->enum('status', ['PUBLICADO', 'INACTIVO'])->default('PUBLICADO');
            $table->timestamps();
            $table->softDeletes();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('estudios');
    }
}
