<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class AddTemplateFliedsToDatosTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::table('datos', function(Blueprint $table) {
            $table->string('telefono');
            $table->string('titulo_desc')->before('descripcion');
            $table->string('ff_icon_1')->nullable();
            $table->string('ff_text_1')->nullable();
            $table->integer('ff_number_1')->nullable();
            $table->string('ff_icon_2')->nullable();
            $table->string('ff_text_2')->nullable();
            $table->integer('ff_number_2')->nullable();
            $table->string('ff_icon_3')->nullable();
            $table->string('ff_text_3')->nullable();
            $table->integer('ff_number_3')->nullable();
            $table->string('ff_icon_4')->nullable();
            $table->string('ff_text_4')->nullable();
            $table->integer('ff_number_4')->nullable();
            
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::table('datos', function(Blueprint $table) {
            $table->dropColumn('telefono');
            $table->dropColumn('titulo_desc');
            $table->dropColumn('ff_icon_1');
            $table->dropColumn('ff_text_1');
            $table->dropColumn('ff_number_1');
            $table->dropColumn('ff_icon_2');
            $table->dropColumn('ff_text_2');
            $table->dropColumn('ff_number_2');
            $table->dropColumn('ff_icon_3');
            $table->dropColumn('ff_text_3');
            $table->dropColumn('ff_number_3');
            $table->dropColumn('ff_icon_4');
            $table->dropColumn('ff_text_4');
            $table->dropColumn('ff_number_4');

        });
    }
}
